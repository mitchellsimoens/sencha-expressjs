module.exports = {
    get BaseRoute () {
        return require('./BaseRoute');
    },

    get ProxyRoute () {
        return require('./ProxyRoute');
    },

    get Route () {
        return require('./Route');
    },

    get Router () {
        return require('./Router');
    },

    get Routerable () {
        return require('./Routerable');
    },

    get SimpleRoute () {
        return require('./SimpleRoute');
    },

    get UglifyRoute () {
        return require('./UglifyRoute');
    }
};
