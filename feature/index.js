'use strict';

module.exports =  {
    get BodyParserable () {
        return require('./BodyParserable');
    },

    get Compressable () {
        return require('./Compressable');
    },

    get Cookieable () {
        return require('./Cookieable');
    },

    get Expressable () {
        return require('./Expressable');
    },

    get FavIconable () {
        return require('./FavIconable');
    },

    get Minifyable () {
        return require('./Minifyable');
    },

    get Multerable () {
        return require('./Multerable');
    },

    get SocketIOable () {
        return require('./SocketIOable');
    },

    get SSLForceable () {
        return require('./SSLForceable');
    }
};
