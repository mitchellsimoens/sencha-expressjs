module.exports = {
    get Response () {
        return require('./Response');
    },

    get Server () {
        return require('./Server');
    },

    get SSLServer () {
        return require('./SSLServer')
    },

    get feature () {
        return require('./feature');
    },

    get route () {
        return require('./route');
    }
};
